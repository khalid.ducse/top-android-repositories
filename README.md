**TOP ANDROID REPOSITORIES**
- Show top android repositories in Github
- REST Api to get data from Github Api
- Recyclerview with pagination
- Used MVVM architecture pattern

Todo (Future update plan):
- Add filter/sort button in repository list to support showing list in different order
- Details page for repository
- Database support for offline browsing
- Add error handling mechanism for different types of error while using REST Api 
- Unit test
- Addition of RxJava, Dagger dependency injection library
