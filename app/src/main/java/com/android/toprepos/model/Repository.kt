package com.android.toprepos.model

import android.os.Parcel
import android.os.Parcelable
import android.os.Parcelable.Creator
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Repository(`in`: Parcel) : Parcelable {
    @SerializedName("owner")
    @Expose
    private val owner: Owner? = null

    @SerializedName("name")
    @Expose
    private val mName: String?

    @SerializedName("description")
    @Expose
    private val mDescription: String?

    @SerializedName("stargazers_count")
    @Expose
    private var mStarsNumber: Float
    fun getmName(): String? {
        return mName
    }

    fun getOwner(): Owner? {
        return owner
    }

    fun getmDescription(): String? {
        return mDescription
    }

    fun getmStarsNumber(): Float {
        return mStarsNumber
    }

    fun setmStarsNumber(mStarsNumber: Float) {
        this.mStarsNumber = mStarsNumber
    }

    inner class Owner {
        @SerializedName("login")
        var login: String? = null

        @SerializedName("avatar_url")
        val avatar_url: String? = null
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(parcel: Parcel, i: Int) {
        parcel.writeString(mDescription)
        parcel.writeString(mName)
        parcel.writeFloat(mStarsNumber)
    }

    companion object {
        @JvmField
        val CREATOR: Creator<Repository?> = object : Creator<Repository?> {
            override fun createFromParcel(`in`: Parcel): Repository? {
                return Repository(`in`)
            }

            override fun newArray(size: Int): Array<Repository?> {
                return arrayOfNulls(size)
            }
        }
    }

    init {
        mDescription = `in`.readString()
        mName = `in`.readString()
        mStarsNumber = `in`.readInt().toFloat()
    }
}
