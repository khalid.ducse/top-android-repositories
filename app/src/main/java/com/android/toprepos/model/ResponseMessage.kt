package com.android.toprepos.model

import com.google.gson.annotations.SerializedName

class ResponseMessage {
    @SerializedName("items")
    private val items: List<Repository?>? = null
    fun getItems(): List<Repository?>? {
        return items
    }
}