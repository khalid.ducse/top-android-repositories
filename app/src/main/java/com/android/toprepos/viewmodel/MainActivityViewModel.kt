package com.android.toprepos.view.ViewModel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.android.toprepos.model.Repository
import com.android.toprepos.model.ResponseMessage
import com.android.toprepos.repository.Api
import com.android.toprepos.repository.Service
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivityViewModel: ViewModel() {
    private val TAG = "MainActivityViewModel"
    private var totalRepo: ArrayList<Repository?>? = null
    private val _repositoriesList = MutableLiveData<ArrayList<Repository?>?>()
    val repositoriesList : LiveData<ArrayList<Repository?>?>
        get() = _repositoriesList

    fun fetch(currPage: Int) {
        Log.d(TAG, "Current Page: $currPage")
        val data: MutableMap<String, String> = HashMap()
        data["q"] = "android"
        data["sort"] = "stars"
        data["order"] = "desc"
        data["per_page"] = "15"
        if (1 <= currPage)
            data["page"] = currPage.toString()
        Api.createApi()
        val apiService: Service? = Api.getService()
        val repositoryListCall: Call<ResponseMessage?>? = apiService?.getRepositoryList(data)
        repositoryListCall?.enqueue(object : Callback<ResponseMessage?> {
            override fun onResponse(
                call: Call<ResponseMessage?>,
                response: Response<ResponseMessage?>
            ) {
                Log.d(TAG, "[onResponse] ${response.raw()}")
                if (response.isSuccessful) {
                    Log.d(TAG, "response: ${response.body()}")
                    if (totalRepo == null) {
                        totalRepo = response.body()?.getItems() as ArrayList<Repository?>?
                    } else {
                        val nextRepositoriesList = response.body()?.getItems()
                        if (nextRepositoriesList != null) {
                            totalRepo?.addAll(nextRepositoriesList)
                        }
                    }
                    _repositoriesList.value = totalRepo
                } else {
                    Log.d(TAG, "parse error")
                }
            }

            override fun onFailure(call: Call<ResponseMessage?>, t: Throwable) {
                Log.d(TAG, "[onFailure] ${t.stackTrace}")
            }
        })
    }
}