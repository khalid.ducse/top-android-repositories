package com.android.toprepos.repository

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object Api {
    private val BASE_URL = "https://api.github.com"
    private var retrofit: Retrofit? = null

    fun createApi(): Retrofit?{
        if(null == retrofit){
            retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        }
        return retrofit
    }

    fun getService(): Service? {
        return retrofit?.create(Service::class.java)
    }
}