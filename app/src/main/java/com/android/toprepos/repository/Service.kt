package com.android.toprepos.repository

import com.android.toprepos.model.ResponseMessage
import com.google.gson.JsonObject
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.QueryMap

interface Service {
    @GET("/search/repositories")
    fun getRepositoryList(@QueryMap(encoded = false) filter: MutableMap<String, String>): Call<ResponseMessage?>?
}