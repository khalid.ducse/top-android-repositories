package com.android.toprepos.adapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.android.toprepos.R
import com.android.toprepos.model.Repository

class RepositoryListAdapter(repositories: List<Repository?>?) : RecyclerView.Adapter<RecyclerView.ViewHolder?>() {

    private val repositories: List<Repository>

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view: View = LayoutInflater.from(parent.context)
                .inflate(R.layout.list_item_layout, parent, false)
        return RepositoryViewHolder(view)
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        if(viewHolder is RepositoryViewHolder) {
            viewHolder.name.setText(repositories[position].getmName())
            viewHolder.description.setText(repositories[position].getmDescription())
        }
    }

    override fun getItemCount(): Int {
        return repositories.size
    }

    class RepositoryViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var name: TextView
        var description: TextView
        init {
            name = itemView.findViewById(R.id.item_title)
            description = itemView.findViewById(R.id.item_description)
        }
    }

    init {
        this.repositories = repositories as List<Repository>
    }
}
