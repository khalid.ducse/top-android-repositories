package com.android.toprepos.view
import android.os.Bundle
import android.widget.ProgressBar
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.toprepos.R
import com.android.toprepos.adapter.RepositoryListAdapter
import com.android.toprepos.model.Repository
import com.android.toprepos.view.ViewModel.MainActivityViewModel

class MainActivity : AppCompatActivity(){

    private var recyclerView: RecyclerView? = null
    private var adapter: RepositoryListAdapter? = null
    private var repositoriesList: ArrayList<Repository?>? = null
    private var currentPage = 1
    private var progressBar: ProgressBar? = null
    private var isLoading = false
    private var isFirstTimeLoad = false
    private val viewModel: MainActivityViewModel by lazy {
        ViewModelProvider(this).get(MainActivityViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        repositoriesList = null
        initLayout()
        registerObserver()
        viewModel.fetch(currentPage)
    }

    private fun registerObserver() {
        viewModel.repositoriesList.observe(this) {
            prepareData(it)
        }
    }

    private fun initLayout(){
        recyclerView = findViewById(R.id.repolist_recyclerview)
        progressBar = findViewById(R.id.repository_progress)
        recyclerView?.setLayoutManager(LinearLayoutManager(this))
        recyclerView?.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val linearLayoutManager: LinearLayoutManager
                = recyclerView.layoutManager as LinearLayoutManager
                if(!isLoading){
                    if(linearLayoutManager.findLastCompletelyVisibleItemPosition() == (repositoriesList?.size?.minus(2)))   {
                        isLoading = true
                        currentPage++
                        loadMoreDataFromApi(currentPage)
                    }
                }
            }
        })
    }

    private fun loadMoreDataFromApi(page: Int) {
        viewModel.fetch(page)
        isLoading = false
    }

    private fun prepareData(updatedRepoList: ArrayList<Repository?>?) {
        repositoriesList = updatedRepoList
        if(!isFirstTimeLoad){
            isFirstTimeLoad = true
            adapter = RepositoryListAdapter(repositoriesList)
            recyclerView?.adapter = adapter
        }else{
            adapter?.notifyDataSetChanged()
        }
    }
}
